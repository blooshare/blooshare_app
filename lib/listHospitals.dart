import 'dart:convert';

import 'package:blooshare_app/models/hospital.dart';
import 'package:flutter/material.dart';

import 'package:http/http.dart' as http;

class ListHospitals extends StatefulWidget {
  @override
  createState() => _ListHospitals();
}

class _ListHospitals extends State {
  var hospitals = new List<Hospital>();

  _getHospitals() async {
    final response = await http.get("http://localhost:5001/hospitals");
    setState(() {
      Iterable list = jsonDecode(response.body);
      hospitals = list.map((model) => Hospital.fromJson(model)).toList();
    });
  }

  @override
  void initState() {
    super.initState();
    _getHospitals();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  build(context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("User List"),
        ),
        body: ListView.builder(
          itemCount: hospitals.length,
          itemBuilder: (context, index) {
            return ListTile(title: Text(hospitals[index].name));
          },
        ));
  }
}
