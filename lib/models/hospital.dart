class Hospital {
  String name;

  Hospital(String name) {
    this.name = name;
  }

  Hospital.fromJson(Map json) : name = json['name'];

  Map toJson() {
    return {'name': name};
  }
}
